package stepDefinition;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class Steps {
	WebDriver dr;
	
	@Given("^the url http://spezicoe\\.wipro\\.com:(\\d+)/opencart(\\d+)/ is launched$")
	public void the_url_http_spezicoe_wipro_com_opencart_is_launched(int arg1, int arg2) throws Throwable {
		System.setProperty("webdriver.chrome.driver", "D:\\20111917\\ASSIGNMENT04\\drivers\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("https://demo.opencart.com");
		
	}

	@When("^the user clicks on My Account and Register buttons$")
	public void the_user_clicks_on_My_Account_and_Register_buttons() throws Throwable {
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		dr.findElement(By.linkText("Register")).click();
		Thread.sleep(4000);
	    	
	}

	@When("^the user enters the mandatory fields$")
	public void the_user_enters_the_mandatory_fields() throws Throwable {
		 File f=new File("D:\\20111917\\ASSIGNMENT04\\Resources\\Registration.xls");
		  Workbook b=Workbook.getWorkbook(f);
		Sheet s=b.getSheet(0);
		int col=s.getColumns();
		String []str=new String[col];
		for(int i=0;i<col;i++)
		{
			Cell c=s.getCell(i, 0);
			str[i]=c.getContents();
		}
		dr.findElement(By.name("firstname")).sendKeys(str[0]);
		dr.findElement(By.name("lastname")).sendKeys(str[1]);
		dr.findElement(By.name("email")).sendKeys(str[2]);
		dr.findElement(By.name("telephone")).sendKeys(str[3]);
		dr.findElement(By.name("password")).sendKeys(str[4]);
		dr.findElement(By.name("confirm")).sendKeys(str[5]);
		Thread.sleep(2000);
	   
	}

	@When("^checks the privacy policy checkbox$")
	public void checks_the_privacy_policy_checkbox() throws Throwable {
		dr.findElement(By.name("agree")).click();
		assertTrue(dr.findElement(By.name("agree")).isSelected());
	}

	@When("^clicks the continue button$")
	public void clicks_the_continue_button() throws Throwable {
		dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]")).click();
	}
	
	@When("^clicks the continue button2$")
	public void clicks_the_continue_button2() throws Throwable {
		dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div[2]/input")).click();
	}

	@Then("^Congratulations! Your new account has been successfully created! message should be displayed$")
	public void congratulations_Your_new_account_has_been_successfully_created_message_should_be_displayed() throws Throwable {
		assertEquals("Your Account Has Been Created!", dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
	}

	@Then("^User should be able to logout$")
	public void user_should_be_able_to_logout() throws Throwable {
		dr.findElement(By.linkText("My Account")).click();
		dr.findElement(By.linkText("Logout")).click();
		dr.close();
	}
	
	@When("^the user clicks on My Account and Login$")
	public void the_user_clicks_on_My_Account_and_Login() throws Throwable {
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/i")).click();
		dr.findElement(By.linkText("Login")).click();
	}

	@When("^the user enters the Email and password$")
	public void the_user_enters_the_Email_and_password() throws Throwable {
		dr.findElement(By.name("email")).sendKeys("moree@123.com");
		dr.findElement(By.name("password")).sendKeys("qwerty123");
	}

	@When("^clicks the Login button$")
	public void clicks_the_Login_button() throws Throwable {
		dr.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
	}

	@When("^clicks the Edit account link$")
	public void clicks_the_Edit_account_link() throws Throwable {
		dr.findElement(By.linkText("Edit Account")).click();
	}

	@When("^enters the new telephone number$")
	public void enters_the_new_telephone_number() throws Throwable {
		dr.findElement(By.name("telephone")).clear();
		dr.findElement(By.name("telephone")).sendKeys("0987654321");
	}

	@Then("^Success: Your account has been successfully updated! should be displayed$")
	public void success_Your_account_has_been_successfully_updated_should_be_displayed() throws Throwable {
	    assertEquals("Success: Your account has been successfully updated.", dr.findElement(By.xpath("//*[@id=\"account-account\"]/div[1]")).getText());
	}
	
	@When("^counts the number of menu links$")
	public void counts_the_number_of_menu_links() throws Throwable {
	    System.out.println(dr.findElements(By.tagName("a")).size());
	}

	@Then("^clicks on each menu link (\\d+) by (\\d+) and takes snapshot$")
	public void clicks_on_each_menu_link_by_and_takes_snapshot(int arg1, int arg2) throws Throwable {
		for(int i=1;i<=8;i++) {
			dr.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li["+i+"]/a")).click();
			File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
		    FileUtils.copyFile(screenshotFile, new File("D:\\20111917\\ASSIGNMENT04\\screenshots\\TC04_"+i+".png"));
		}
	}
	
	@When("^clicks on store link available on top left corner$")
	public void clicks_on_store_link_available_on_top_left_corner() throws Throwable {
		dr.findElement(By.linkText("Your Store")).click();
	}

	@Then("^Home page should be displayed$")
	public void home_page_should_be_displayed() throws Throwable {
	    assertEquals("https://demo.opencart.com/index.php?route=common/home", dr.getCurrentUrl());
	}

	@Then("^clicks on Brands link available on bottom navbar under Extras$")
	public void clicks_on_Brands_link_available_on_bottom_navbar_under_Extras() throws Throwable {
		dr.findElement(By.linkText("Brands")).click();
	}

	@Then("^Find your favourite Brand page should be displayed$")
	public void find_your_favourite_Brand_page_should_be_displayed() throws Throwable {
		assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer", dr.getCurrentUrl());
	}
}
